# TOPTAL: Marco Sanabria Home Project

This project made up of three separate sub-projects:
- **UI Automation**
- **API Automation**
- **Performance Automation**

For the first two projects, the automation is done using **Cypress** and for the third project, the automation is done using **K6**.

---------------------------------------------------------

## Setup and Configuration

#### Prerequisites
- Node JS version 16.x
- Yarn version 1.22.x


In order to set up the project, you need to install all the dependencies specified in the package.json file by running:
```
yarn install
```

---------------------------------------------------------

## UI Automation & API Automation using Cypress Test Runner

* To run tests locally using the cypress test runner, you can to run the following command:
```
yarn cy:open
```

* To run all tests and generate the report, you can run the following command:
```
yarn cy:test:run
```

---------------------------------------------------------

## Performance Automation
