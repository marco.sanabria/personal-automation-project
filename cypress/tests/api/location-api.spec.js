/// <reference types="Cypress" />

describe('Location API', () => {
  context('Positive Scenarios', () => {
    it('should retrieve a match', () => {
      cy.getMapboxDirections(
        'cycling',
        '-84.518641,39.134270;-84.512023,39.102779',
        Cypress.env('MAPBOX_ACCESS_TOKEN')
      ).then(response => {
        expect(response.status).to.eq(200);
        expect(response.body).to.have.property('routes');
        expect(response.body).to.have.property('waypoints');
        expect(response.body).to.have.property('code');
        expect(response.body.routes).to.have.length.gte(1);
      });
    });

    it('should create a style', () => {
      cy.fixture('mapbox-style').then(style => {
        cy.addMapboxStyle(style).then(response => {
          expect(response.status).to.eq(201);
          expect(response.statusText).to.eq('Created');
          expect(response.body).to.have.property('id');
          expect(response.body).to.have.property('owner');
          expect(response.body).to.have.property('name');
          expect(response.body).to.have.property('created');
        });
      });
    });

    context('Update & Delete a Style', () => {
      beforeEach(() => {
        cy.fixture('mapbox-style').then(style => {
          cy.addMapboxStyle(style).its('body').as('newStyle');
        });
      });

      afterEach(function () {
        cy.deleteMapboxStyle(this.newStyle.id, { failOnStatusCode: false });
      });

      it('should update a style', () => {
        cy.get('@newStyle').then(style => {
          cy.fixture('update-mapbox-style').then(updatedStyle => {
            cy.request({
              method: 'PATCH',
              url: `${Cypress.env('requestBaseUrl')}/styles/v1/${Cypress.env('MAPBOX_USERNAME')}/${style.id}`,
              qs: { access_token: Cypress.env('MAPBOX_ACCESS_TOKEN') },
              body: updatedStyle,
            }).then(response => {
              expect(response.status).to.eq(200);
              expect(response.statusText).to.eq('OK');
              expect(response.body.name).to.eq(updatedStyle.name);
            });
          });
        });
      });

      it('should delete a style', () => {
        cy.get('@newStyle').then(style => {
          cy.deleteMapboxStyle(style.id).then(response => {
            expect(response.status).to.eq(204);
            expect(response.statusText).to.eq('No Content');
          });
        });
      });
    });
  });

  context('Negative Scenarios', () => {
    it('should throw authentication error', () => {
      cy.getMapboxDirections('cycling', '-84.518641,39.134270;-84.512023,39.102779', 'invalid_token', {
        failOnStatusCode: false,
      }).then(response => {
        expect(response.status).to.eq(401);
        expect(response.body).to.have.property('message');
        expect(response.body.message).to.eq('Not Authorized - Invalid Token');
        expect(response.statusText).to.eq('Unauthorized');
      });
    });

    it('should throw a 422 HTTP Status code on malformed coordinates', () => {
      cy.getMapboxDirections('cycling', '-84.518641,39.134270;-84.512023', Cypress.env('MAPBOX_ACCESS_TOKEN'), {
        failOnStatusCode: false,
      }).then(response => {
        expect(response.status).to.eq(422);
        expect(response.body).to.have.property('message');
        expect(response.body.message).to.include('Coordinate is invalid');
        expect(response.statusText).to.eq('Unprocessable Entity');
      });
    });

    it('should failt to update a style with invalid id', () => {
      cy.fixture('update-mapbox-style').then(updatedStyle => {
        cy.request({
          method: 'PATCH',
          url: `${Cypress.env('requestBaseUrl')}/styles/v1/${Cypress.env('MAPBOX_USERNAME')}/invalidID`,
          qs: { access_token: Cypress.env('MAPBOX_ACCESS_TOKEN') },
          body: updatedStyle,
          failOnStatusCode: false,
        }).then(response => {
          expect(response.status).to.eq(404);
          expect(response.statusText).to.eq('Not Found');
          expect(response.body).to.have.property('message');
          expect(response.body.message).to.eq('Style not found');
        });
      });
    });
  });
});
