/// <reference types="Cypress" />

import { ProductPage } from '../../../page-objects/products/product.page.object';
import { ProductsPage } from '../../../page-objects/products/products.page.object';
import { LayerCartModal } from '../../../page-objects/shopping-cart/layer-cart.page.object';

describe('Product View', () => {
  const productsPage = new ProductsPage();
  const layerCart = new LayerCartModal();
  const productPage = new ProductPage();

  beforeEach(() => {
    cy.login(Cypress.env('TOPTAL_USER'), Cypress.env('PASSWORD'));
    productsPage.visit();
    productsPage.goToFirstProduct();
  });

  it('should add a product to the shopping cart', () => {
    cy.get('@productName').then(productName => {
      productPage.addToCart();
      layerCart.elements
        .getModal()
        .should('be.visible')
        .and($modal => {
          expect($modal.text())
            .to.contain('Product successfully added to your shopping cart')
            .and.to.contain('Continue shopping')
            .and.to.contain('Proceed to checkout')
            .and.to.contain('There is 1 item in your cart')
            .and.to.contain(productName.replace(/(\t|\n)/gi, ''));
        });
    });
    productsPage.elements
      .getCartButton()
      .should($cartButton =>
        expect($cartButton.text().replace(/\t/gi, '').replace(/\n/gi, ' ')).to.include('1 Product')
      );
  });
});
