/// <reference types="Cypress" />

import { ProductPage } from '../../../page-objects/products/product.page.object';
import { ProductsPage } from '../../../page-objects/products/products.page.object';
import { LayerCartModal } from '../../../page-objects/shopping-cart/layer-cart.page.object';

describe('Products View', () => {
  const productsPage = new ProductsPage();
  const layerCart = new LayerCartModal();

  beforeEach(() => {
    cy.login(Cypress.env('TOPTAL_USER'), Cypress.env('PASSWORD'));
    productsPage.visit();
  });

  [{ criteria: 'dress' }, { criteria: 't-shirt' }].forEach(item => {
    it(`should search products by criteria ${item.criteria}`, () => {
      productsPage.searchByCriteria(`${item.criteria}{enter}`);
      cy.contains(`Search "${item.criteria}"`, { matchCase: false }).should('be.visible');
      productsPage.elements
        .getProducts()
        .should('have.length.greaterThan', 0)
        .find('.product-name')
        .each(($el, index, $list) => {
          cy.wrap($el).scrollIntoView().invoke('text').should('match', RegExp(item.criteria, 'i'));
        });
    });
  });

  it('should search products by pants', () => {
    productsPage.searchByCriteria('pants{enter}');
    cy.contains('#center_column', 'Search')
      .should($element => {
        const text = $element
          .text()
          .split('\n')
          .map(value => value.trim())
          .filter(value => value !== '' && value !== '\t')[0];
        expect(text.toUpperCase()).to.equal('SEARCH');
      })
      .should('be.visible');
    cy.contains('No results were found for your search "pants"').should('be.visible');
    cy.contains('0 results have been found').should('be.visible');
    productsPage.elements.getProducts().should('have.length', 0);
  });

  it('should add a product to the cart', () => {
    productsPage.addProductToCart();
    cy.get('@productName').then(productName => {
      layerCart.elements
        .getModal()
        .should('be.visible')
        .and($modal => {
          expect($modal.text())
            .to.contain('Product successfully added to your shopping cart')
            .and.to.contain('Continue shopping')
            .and.to.contain('Proceed to checkout')
            .and.to.contain('There is 1 item in your cart')
            .and.to.contain(productName.replace(/(\t|\n)/gi, ''));
        });
    });
    productsPage.elements
      .getCartButton()
      .should($cartButton =>
        expect($cartButton.text().replace(/\t/gi, '').replace(/\n/gi, ' ')).to.include('1 Product')
      );
  });
});
