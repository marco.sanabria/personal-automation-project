/// <reference types="Cypress" />

import { LoginPage } from '../../../page-objects/login/login.page.object';

describe('Login', () => {
  const loginPage = new LoginPage();

  context('Positive Scenarios', () => {
    it('should login with correct credential', () => {
      cy.login(Cypress.env('TOPTAL_USER'), Cypress.env('PASSWORD'));
      cy.url().should('include', '/index.php?controller=my-account');
      cy.contains('My account').should('be.visible');
    });

    context('Logout', () => {
      beforeEach(() => {
        cy.login(Cypress.env('TOPTAL_USER'), Cypress.env('PASSWORD'));
      });
      it('should logout', () => {
        loginPage.logout();
        cy.url().should('include', '/index.php?controller=authentication');
        cy.contains('Sign in').should('be.visible');
        loginPage.elements.signInButton().should('be.visible');
      });
    });
  });

  context('Negative Scenarios', () => {
    it('should not login with the wrong email', () => {
      cy.login('wrongEmail@test.com', Cypress.env('PASSWORD'));
      cy.url().should('include', '/index.php?controller=authentication');
      cy.contains('Authentication').should('be.visible');
      cy.get('.alert-danger').should(element => {
        expect(element).to.contain('There is 1 error');
        expect(element).to.contain('Authentication failed.');
      });
    });

    it('should not login with the wrong password', () => {
      cy.login(Cypress.env('TOPTAL_USER'), 'wrongPassword');
      cy.url().should('include', '/index.php?controller=authentication');
      cy.contains('Authentication').should('be.visible');
      cy.get('.alert-danger').should(element => {
        expect(element).to.contain('There is 1 error');
        expect(element).to.contain('Authentication failed.');
      });
    });

    it('should not login without credentials', () => {
      loginPage.visit();
      loginPage.elements.signInButton().click();
      cy.url().should('include', '/index.php?controller=authentication');
      cy.contains('Authentication').should('be.visible');
      cy.get('.alert-danger').should(element => {
        expect(element).to.contain('There is 1 error');
        expect(element).to.contain('An email address required.');
      });
    });

    it('should not login without password', () => {
      loginPage.visit();
      loginPage.elements.getUsername().type(Cypress.env('TOPTAL_USER'));
      loginPage.elements.signInButton().click();
      cy.url().should('include', '/index.php?controller=authentication');
      cy.contains('Authentication').should('be.visible');
      cy.get('.alert-danger').should(element => {
        expect(element).to.contain('There is 1 error');
        expect(element).to.contain('Password is required.');
      });
    });
  });
});
