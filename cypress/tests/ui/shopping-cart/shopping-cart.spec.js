/// <reference types="Cypress" />

import { MyAccount } from '../../../page-objects/my-account/my-account.page.object';
import { ProductsPage } from '../../../page-objects/products/products.page.object';
import { ShoppingCartPage } from '../../../page-objects/shopping-cart/shopping-cart.page.object';

describe('Shopping Cart', () => {
  const shoppingCart = new ShoppingCartPage();
  const productsPage = new ProductsPage();

  function navigateThroughStages(stages, index = 0, shouldAssert = true) {
    if (shouldAssert) shoppingCart.getCurrentStageName().should('contain', stages[index]);
    if (index === stages.length - 2) shoppingCart.agreeToShippingTerms();
    if (index === stages.length - 1) return;
    shoppingCart.proceedToCheckout();
    return navigateThroughStages(stages, index + 1);
  }

  beforeEach(() => {
    cy.login(Cypress.env('TOPTAL_USER'), Cypress.env('PASSWORD'));
    // new MyAccount().addAddress();
  });

  const stages = ['01. Summary', '02. Sign in', '03. Address', '04. Shipping', '05. Payment'];

  it('should navigate to the shopping cart', () => {
    shoppingCart.visit();
    cy.url().should('include', shoppingCart.url);
    cy.contains('Your shopping cart is empty').should('be.visible');
    stages.forEach(stage => cy.contains(stage).should('be.visible'));
  });

  context('Checkout Process', () => {
    beforeEach(() => {
      productsPage.visit();
      productsPage.addProductToCart();
      shoppingCart.visit();
    });

    it('should navigate through the checkout process', () => {
      navigateThroughStages(stages.filter(stage => stage !== '02. Sign in'));
    });

    it('should complete checkout process', () => {
      navigateThroughStages(stages.filter((stage) => stage !== '02. Sign in'));
      cy.get('@productName').then((productName) => cy.contains(productName.replace(/(\t|\n)/gi, '')).should('exist'));
      shoppingCart.selectPayBankWire();
      cy.contains('Bank-wire payment').should('be.visible');
      shoppingCart.confirmOrder();
      cy.contains('Your order on My Store is complete.').should('be.visible');
      shoppingCart.elements.getCartButton().invoke('text').should('contain', '(empty)');
    });
  });
});
