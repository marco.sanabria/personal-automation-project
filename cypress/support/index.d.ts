declare namespace Cypress {
  interface Chainable<Subject> {
    /**
     * Custom command to select DOM element by data-cy attribute.
     * @example
     *    cy.dataCy('greeting')
     */
    login(username: string, password: string): Chainable<Subject>;

    getMapboxDirections(profile: string, coordinates: string, access_token: string): Chainable<Subject>;

    addMapboxStyle(style: object): Chainable<Subject>;

    deleteMapboxStyle(styleId: string): Chainable<Subject>;
  }
}
