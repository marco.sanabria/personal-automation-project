// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'
import addContext from 'mochawesome/addContext';

// Alternatively you can use CommonJS syntax:
// require('./commands')

beforeEach(() => {
  cy.clearLocalStorage();
});

Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false;
});

Cypress.on('test:after:run', (test, runnable) => {
  if (test.state === 'failed') {
    const specName = Cypress.spec.name;
    const parentName = getParentName(runnable);
    const screenshot = `assets/${specName}/${parentName} -- ${test.title} (failed).png`;
    addContext({ test }, screenshot);
    addContext({ test }, `assets/videos/${specName}.mp4`);
  }
});

function getParentName(runnable, titles = []) {
  if (runnable.parent.title) {
    titles.push(runnable.parent.title);
    return getParentName(runnable.parent, titles);
  }
  return titles.reverse().join(' -- ');
}
