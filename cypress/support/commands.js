// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//

import { LoginPage } from '../page-objects/login/login.page.object';

// -- This is a parent command --
Cypress.Commands.add('login', (username, password) => {
  const loginPage = new LoginPage();
  loginPage.visit();
  loginPage.login(username, password);
});

Cypress.Commands.add('getMapboxDirections', (profile, coordinates, access_token, options = {}) => {
  cy.request({
    method: 'GET',
    url: `${Cypress.env('requestBaseUrl')}/directions/v5/mapbox/${profile}/${coordinates}`,
    qs: {
      geometries: 'geojson',
      access_token: access_token,
    },
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    ...options,
  });
});

Cypress.Commands.add('addMapboxStyle', style => {
  cy.request({
    method: 'POST',
    url: `${Cypress.env('requestBaseUrl')}/styles/v1/${Cypress.env('MAPBOX_USERNAME')}`,
    qs: { access_token: Cypress.env('MAPBOX_ACCESS_TOKEN') },
    body: style,
  });
});

Cypress.Commands.add('deleteMapboxStyle', (styleId, options) => {
  cy.request({
    method: 'DELETE',
    url: `${Cypress.env('requestBaseUrl')}/styles/v1/${Cypress.env('MAPBOX_USERNAME')}/${styleId}`,
    qs: { access_token: Cypress.env('MAPBOX_ACCESS_TOKEN') },
    ...options,
  });
});

//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
