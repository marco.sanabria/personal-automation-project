/// <reference types="Cypress" />

export class LayerCartModal {
  elements = {
    getModal: () => cy.get('#layer_cart'),
    getContinueShoppingButton: () => this.elements.getModal().contains('Continue shopping'),
    getProceedToCheckoutButton: () => this.elements.getModal().contains('Proceed to checkout'),
  };

  continueShopping() {
    this.elements.getContinueShoppingButton().click();
  }

  proceedToCheckout() {
    this.elements.getProceedToCheckoutButton().click();
  }
}
