/// <reference types="Cypress" />

import { BasePage } from '../base.page.object';

export class ShoppingCartPage extends BasePage {
  url = '/index.php?controller=order';
  elements = {
    ...BasePage.elements,
    getCartNavigation: () => cy.get('.cart_navigation'),
    getBankWire: () => cy.get('.bankwire'),
  };

  visit() {
    cy.visit(this.url);
  }

  getCurrentStageName() {
    return cy
      .get('#order_step')
      .find('.step_current')
      .invoke('text')
      .invoke('replace', /(\t|\n)/gi, '');
  }

  agreeToShippingTerms() {
    cy.get('input[type="checkbox"]').check();
  }

  proceedToCheckout() {
    this.elements.getCartNavigation().scrollIntoView().find('.standard-checkout, button').click();
  }

  selectPayBankWire() {
    this.elements.getBankWire().click();
  }

  confirmOrder() {
    cy.contains('button', 'I confirm my order').click();
  }
}
