/// <reference types="Cypress" />

export class MyAccount {
  addAddress() {
    cy.visit('/index.php?controller=address');
    cy.fixture('my-address').then(data => {
      for (const key in data) {
        if (key === 'state') cy.get(data[key].locator).select(data[key].value);
        cy.get(data[key].locator).type(data[key].value);
      }
      cy.get('#submitAddress').click();
    });
  }
}
