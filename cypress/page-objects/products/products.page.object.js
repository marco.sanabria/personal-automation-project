/// <reference types="Cypress" />

import { BasePage } from '../base.page.object';

export class ProductsPage extends BasePage {
  constructor() {
    super();
  }

  elements = {
    ...BasePage.elements,
    getProducts: () => cy.get('.product-container'),
    getAutoComplete: () => cy.get('.ac_results li'),
  };

  visit() {
    cy.visit('/index.php');
  }

  searchByCriteria(criteria) {
    this.elements.getSearchInput().type(criteria);
  }

  addProductToCart() {
    this.getFirstProduct().trigger('mouseover').contains('Add to cart').click();
  }

  getAliasedProduct(alias) {
    return cy.get(alias);
  }

  getFirstProduct() {
    return this.elements
      .getProducts()
      .first()
      .then($product => {
        cy.wrap($product).find('.product-name').invoke('text').as('productName');
        return cy.wrap($product);
      });
  }

  quickView() {
    this.getFirstProduct().trigger('mouseover').contains('Quick view').click();
  }

  goToFirstProduct() {
    this.getFirstProduct().click();
  }
}
