/// <reference types="Cypress" />

import { BasePage } from '../base.page.object';

export class ProductPage extends BasePage {
  constructor() {
    super();
  }

  elements = {
    ...BasePage.elements,
    getAddToCartButton: () => cy.contains('Add to cart'),
  };

  addToCart() {
    this.elements.getAddToCartButton().click();
  }
}
