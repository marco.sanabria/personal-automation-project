/// <reference types="Cypress" />

export class LoginPage {
  elements = {
    getUsername: () => cy.get('#email'),
    getPassword: () => cy.get('#passwd'),
    signInButton: () => cy.get('#SubmitLogin'),
  };

  visit() {
    cy.visit('/index.php?controller=authentication&back=my-account');
  }

  login(username, password) {
    this.elements.getUsername().type(username);
    this.elements.getPassword().type(password, { log: false });
    this.elements.signInButton().click();
  }

  logout() {
    cy.contains('Sign out').click();
  }
}
