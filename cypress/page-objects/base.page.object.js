/// <reference types="Cypress" />

export class BasePage {
  static elements = {
    getSearchInput: () => cy.get('#search_query_top'),
    getCartButton: () => cy.get('.shopping_cart a'),
  };
}
